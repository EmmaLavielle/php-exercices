<!-- ##Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres.
Tous les paramètres doivent avoir une valeur par défaut. -->

<?php

function somme ($nombre1 = 1, $nombre2 = 2, $nombre3 = 3)
{
    return $nombre1 + $nombre2 + $nombre3;
}

echo somme();