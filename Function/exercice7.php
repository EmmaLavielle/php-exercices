<!-- ##Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur :

    Homme
    Femme

La fonction doit renvoyer en fonction des paramètres :

    Vous êtes un homme et vous êtes majeur
    Vous êtes un homme et vous êtes mineur
    Vous êtes une femme et vous êtes majeur
    Vous êtes une femme et vous êtes mineur

Gérer tous les cas. -->

<?php

function f ()
    {
    $age = 25;
    $genre = "femme";

        if($age < 18)
            {
                echo "Vous êtes mineur(e)";
            }
        else if($age >= 18)
            {
                echo "Vous êtes majeur(e)";
            }

        if($genre == "homme")
            {
                echo " et vous êtes un homme";
            }
        else if($genre == "femme")
            {
                echo " et vous êtes une femme";
            }
    }

echo f();
