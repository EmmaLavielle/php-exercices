<!-- ##Exercice 2 Créer un formulaire demandant le nom et le prénom. Ce formulaire doit rediriger vers la page user.php avec la méthode POST. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
    <div>
        <label for="nom">Votre nom : </label>
        <input name="nom" id="nom">
    </div>
    <div>
        <label for="prenom">Votre prénom : </label>
        <input name="prenom">
    </div>
    <div>
        <button>Envoyer</button>
    </div>
    </form>
</body>
</html>
