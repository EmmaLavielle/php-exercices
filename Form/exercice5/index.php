<!-- ##Exercice 5 Créer un formulaire sur la page index.php avec :

Une liste déroulante pour la civilité (Mr ou Mme)
Un champ texte pour le nom
Un champ texte pour le prénom

Ce formulaire doit rediriger vers la page index.php.
Vous avez le choix de la méthode. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
    <div>
        <SELECT name="civilite" size="1">
        <OPTION>Mr
        <OPTION>Mme
        </SELECT>
    </div>
    <div>
        <label for="nom">Votre nom : </label>
        <input name="nom" id="nom">
    </div>
    <div>
        <label for="prenom">Votre prénom : </label>
        <input name="prenom">
    </div>
    <div>
        <button>Envoyer</button>
    </div>
    </form>
</body>
</html>

<?php
echo $_POST['civilite'] . " ";
echo $_POST['nom'] . " ";
echo $_POST['prenom'];
?>