<!-- ##Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau. -->

<?php
$HautsDeFrance = array(
    02 => "Aisne",
    59 => "Nord",
    60 => "Oise",
    62 => "Pas-de-Calais",
    80 => "Somme"
);

foreach($HautsDeFrance as $element){
    echo $element . " ";
}
