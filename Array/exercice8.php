<!-- ##Exercice 8 Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau. -->

<?php

$mois = array
(   "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "aout",
    "septembre",
    "octobre",
    "novembre",
    "décembre");

for($i = 0; $i <= sizeof($mois); $i++){
    echo $mois[$i] . " ";
}