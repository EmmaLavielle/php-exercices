<!-- ##Exercice 3 Faire une page exercice3.php. Tester sur cette page que tous les paramètres de cette URL existent et les afficher: exercice3.php?dateDebut=2/05/2016&dateFin=27/11/2016 -->

<?php
$dateDebut = "2/05/2016";
$dateFin = "27/11/2016";

if(isset($dateDebut)) {
    echo 'La variable $dateDebut existe et vaut : ' . $dateDebut . "<br>";
    }
if(isset($dateFin)) {
    echo 'La variable $dateFin existe et vaut : ' . $dateFin . "<br>";
    }
