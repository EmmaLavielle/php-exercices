<!-- ##Exercice 6 Faire une page exercice6.php. Tester sur cette page que tous les paramètres de cette URL existent et les afficher: exercice6.php?batiment=12&salle=101 -->

<?php
$batiment = "12";
$salle = "101";

if(isset($batiment)) {
    echo 'La variable $batiment existe et vaut : ' . $batiment . "<br>";
    }
if(isset($salle)) {
    echo 'La variable $salle existe et vaut : ' . $salle . "<br>";
    }