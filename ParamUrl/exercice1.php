<!-- #PHP - Les paramètres d'URL ##Exercice 1 Faire une page exercice1.php. Tester sur cette page que tous les paramètres de cette URL existent et les afficher: exercice1.php?nom=Nemare&prenom=Jean -->

<?php
$nom = Nemare;
$prenom = Jean;

if(isset($nom)) {
    echo 'La variable $nom existe et vaut : ' . $nom . "<br>";
    }
if(isset($prenom)) {
    echo 'La variable $prenom existe et vaut : ' . $prenom;
    }
