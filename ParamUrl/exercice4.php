<!-- ##Exercice 4 Faire une page exercice4.php. Tester sur cette page que tous les paramètres de cette URL existent et les afficher: exercice4.php?langage=PHP&serveur=LAMP -->

<?php
$langage = "PHP";
$serveur = "LAMP";

if(isset($langage)) {
    echo 'La variable $langage existe et vaut : ' . $langage . "<br>";
    }
if(isset($serveur)) {
    echo 'La variable $serveur existe et vaut : ' . $serveur . "<br>";
    }